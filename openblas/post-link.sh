# Remove bundled files if they already exist in the system
if ldconfig -p | grep libgfortran.so.3 &> /dev/null; then
    rm $PREFIX/lib/libgfortran.so.3*
    rm $PREFIX/lib/libquadmath.so.0*
fi
