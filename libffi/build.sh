if [[ $(uname) == "Linux" ]]; then
  # this changes the install dir from ${PREFIX}/lib64 to ${PREFIX}/lib
  sed -i 's:@toolexeclibdir@:$(libdir):g' Makefile.in */Makefile.in
  sed -i 's:@toolexeclibdir@:${libdir}:g' libffi.pc.in
fi

./configure \
    --prefix="${PREFIX}" \
    --includedir="${PREFIX}/include" \
    --disable-docs \
    --disable-debug \
    --disable-dependency-tracking \
  || { cat config.log; exit 1;}

make
make check
make install
