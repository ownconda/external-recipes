sed -i "s/__version__ = get_version(__file__)/__version__ = '$PKG_VERSION'/g" conda/__init__.py
echo $PKG_VERSION > conda/.version
$PYTHON -m pip install -I --no-deps .
rm -rf "$SP_DIR/conda/shell/*.exe"
$PYTHON -m conda init --install
