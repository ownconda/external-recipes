checker = 'webbot'
url = 'https://www.python.org/downloads/'
pattern = r'3\.7\.\d+'

import re


def find_version(soup):
    section = soup.find('section', class_='main-content')
    div = section.find('div', class_='download-list-widget')
    items = div.find('ol', class_='list-row-container').find_all('li')
    releases = [li.find('span', class_='release-number').find('a').text for li in items]
    version = None
    for release in releases:
        match = re.search(pattern, release)
        if match is not None:
            version = match.group(0)
            # The list is sorted so we can break after the first match:
            break
    assert version
    return version
