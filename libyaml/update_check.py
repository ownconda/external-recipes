checker = 'webbot'
url = 'http://pyyaml.org/wiki/LibYAML'

import re

def find_version(soup):
    # Find version from this paragraph:
    # <p>
    # The current release of LibYAML: <strong>0.1.5 (2014-02-04)</strong>.
    # </p>
    re_current = re.compile('The current release of LibYAML')
    text = soup.find(string=re_current).next.text
    version = text.split(' ')[0]
    return version
