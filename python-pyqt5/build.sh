if [ `uname` == Darwin ]; then
    export DYLD_FALLBACK_LIBRARY_PATH=$PREFIX/lib
    MAKE_JOBS=$(sysctl -n hw.ncpu)
fi

if [ `uname` == Linux ]; then
    MAKE_JOBS=$CPU_COUNT
fi

pushd pyqt
$PYTHON configure.py \
        --confirm-license \
        --assume-shared \
        --qmake $PREFIX/bin/qmake-qt5
make -j $MAKE_JOBS
make install
popd


pushd pyqtwebengine
$PYTHON configure.py --qmake $PREFIX/bin/qmake-qt5
make -j $MAKE_JOBS
make install
popd

# pip shall think that we only installed one package
head -n -3 $SP_DIR/PyQtWebEngine-${PKG_VERSION}.dist-info/RECORD >> $SP_DIR/PyQt5-${PKG_VERSION}.dist-info/RECORD
rm -rf $SP_DIR/PyQtWebEngine-${PKG_VERSION}.dist-info/
