checker = 'webbot'
url = 'https://www.riverbankcomputing.com/software/pyqt/download5'

import re

def find_version(soup):
    filename = soup.find('table', class_='download').find('a').text
    match = re.match(r'PyQt5_gpl-(\d+\.\d+(\.\d+)?).(tar.gz|zip)', filename)
    return match.group(1)
