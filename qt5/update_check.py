checker = 'custom'

# The QT download server has one folder for "major.minor" version and within
# that folder the actual (patch) releases, so we have to look into two
# directory listings:
url = 'https://download.qt.io/official_releases/qt/'
regexp_folder = r'(\d+\.\d+(\.\d+)?)/'


import os.path
import re

from pip._vendor.packaging.version import parse as parse_version
import bs4
import requests


def get_latest_version():
    # Find latest base version in "url"
    v = _find_latest_version(url)
    # Find latest version in "url/{major}.{minor}"
    v = _find_latest_version(os.path.join(url, v))
    return v


def _find_latest_version(url):
    r = requests.get(url)
    soup = bs4.BeautifulSoup(r.text, 'html.parser')

    re_dir = re.compile(regexp_folder)
    latest_version = parse_version('0')
    for row in soup.find('table')('tr'):
        td = row('td')
        if not td:
            continue

        match = re_dir.match(td[1].find('a').text)
        if match is None:
            continue

        version = parse_version(match.group(1))
        latest_version = max(version, latest_version)

    return str(latest_version)
