# Main variables
# --------------
BIN=$PREFIX/lib/qt5/bin
QTCONF=$BIN/qt.conf
VER=$PKG_VERSION
CXXFLAGS=-fno-delete-null-pointer-checks
# Qt/Chromium needs "python" to be Python 2:
# PATH=$(dirname $(which python2)):$PATH


# Compile
# -------
chmod +x configure

# This might only work on Linux.  Check
# https://github.com/conda/conda-recipes/tree/master/qt5 for OS X support

./configure -prefix $PREFIX \
            -bindir $PREFIX/lib/qt5/bin \
            -headerdir $PREFIX/include/qt5 \
            -archdatadir $PREFIX/lib/qt5 \
            -datadir $PREFIX/share/qt5 \
            -opensource \
            -confirm-license \
            -release \
            -shared \
            -silent \
            -I $PREFIX/include \
            -I $PREFIX/include/mariadb \
            -L $PREFIX/lib \
            -nomake examples \
            -nomake tests \
            -qt-pcre \
            -qt-libjpeg \
            -qt-xcb \
            -webengine-proprietary-codecs

LD_LIBRARY_PATH=$PREFIX/lib make --quiet -j $CPU_COUNT
make install


# Post build setup
# ----------------

# Remove unneeded files
rm -rf $PREFIX/share/qt5

# Make symlinks of binaries in $BIN to $PREFIX/bin
for file in $BIN/*
do
    ln -sfv ../lib/qt5/bin/$(basename $file) $PREFIX/bin/$(basename $file)
    ln -sfv ../lib/qt5/bin/$(basename $file) $PREFIX/bin/$(basename $file)-qt5
done

# Remove static libs
rm -rf $PREFIX/lib/*.a

# Add qt.conf file to the package to make it fully relocatable
cp $RECIPE_DIR/qt.conf $BIN/

# Copy resources for QtWebEngine
RES_DIR=$PREFIX/share/qt5/resources/
TRN_DIR=$PREFIX/share/qt5/translations/
mkdir -p $RES_DIR
mkdir -p $TRN_DIR
cp qtwebengine/src/core/release/icudtl.dat $RES_DIR
cp qtwebengine/src/core/release/*.pak $RES_DIR
cp -r qtwebengine/src/core/release/qtwebengine_locales $TRN_DIR/
