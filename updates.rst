3rd party package updates
=========================

.. This is a template for an email to notify all co-workers when we updated our
.. third party packages

Hi all,

there are new 3rd party packages in our **staging** channel.

Important updates:

python-numpy 1.16.2 (1.15.4):
  Has some new and expired deprecations:
  https://docs.scipy.org/doc/numpy/release.html

python-pytest 4.4.0 (4.2.0):
  Minor release with many new features:
  https://docs.pytest.org/en/latest/changelog.html

python-pyyaml 5.1 (3.13):
  Major release: https://github.com/yaml/pyyaml/blob/master/CHANGES

  "yaml.load()" is now deprecated since it's unsafe:
  https://github.com/yaml/pyyaml/pull/257

Expected merge to **stable**: Monday, DD. MMMM YYYY

Please let me know if there are any problems so that we can roll back
problematic updates.

Cheers,
Stefan


New packages
------------


Removed packages
----------------


Updated packages
----------------

ca-certificates 2019.3.9 (2018.11.29):
  https://github.com/certifi/python-certifi/commits/master

cmake 3.14.2 (3.13.4):
  https://cmake.org/cmake/help/latest/release/index.html

freetype 2.10.0 (2.9.1):
  https://www.freetype.org/index.html#news

libicu 64.1 (63.1):
  http://site.icu-project.org/download/

libyaml 0.2.2 (0.2.1):
  https://github.com/yaml/libyaml/blob/master/announcement.msg

mariadb-connector-c 3.1.0 (3.0.3):
  https://mariadb.com/kb/en/library/mariadb-connectorc-changelogs/

patchelf 0.10 (0.9.0)

python-3.7 3.7.3 (3.7.2):
  https://docs.python.org/3.7/whatsnew/changelog.html

python-attrs 19.1.0 (18.2.0):
  http://www.attrs.org/en/stable/changelog.html

python-certifi 2019.3.9 (2018.11.29):
  https://github.com/certifi/python-certifi/commits/master

python-cffi 1.12.2 (1.11.5):
  https://cffi.readthedocs.io/en/latest/whatsnew.html

python-conda 4.6.12 (4.6.8):
  https://github.com/conda/conda/releases

python-conda-build 3.17.8 (3.13.0):
  https://github.com/conda/conda-build/releases

python-conda-verify 3.2.1 (3.1.1):
  https://github.com/conda/conda-verify/blob/master/CHANGELOG.txt

python-coverage 4.5.3 (4.5.2):
  https://coverage.readthedocs.io/en/latest/changes.html

python-cryptography 2.6.1 (2.5):
  https://cryptography.io/en/latest/changelog/

python-decorator 4.4.0 (4.3.2):
  https://github.com/micheles/decorator/blob/master/CHANGES.md

python-gitlab 1.8.0 (1.7.0):
  http://python-gitlab.readthedocs.io/en/stable/release_notes.html

python-isort 4.3.17 (4.3.4):
  https://github.com/timothycrosley/isort/blob/develop/CHANGELOG.md

python-jinja2 2.10.1 (2.10):
  http://jinja.pocoo.org/docs/latest/changelog/

python-markupsafe 1.1.1 (1.1.0):
  https://github.com/pallets/markupsafe/blob/master/CHANGES.rst

python-more-itertools 7.0.0 (5.0.0):
  https://more-itertools.readthedocs.io/en/latest/versions.html

python-networkx 2.3 (2.2):
  https://networkx.github.io/documentation/stable/news.html

python-numpy 1.16.2 (1.15.4):
  https://docs.scipy.org/doc/numpy/release.html

python-pbr 5.1.3 (5.1.2):
  https://docs.openstack.org/pbr/latest/user/history.html

python-pluggy 0.9.0 (0.8.1):
  https://github.com/pytest-dev/pluggy/blob/master/CHANGELOG.rst

python-psutil 5.6.1 (5.5.0):
  https://github.com/giampaolo/psutil/blob/master/HISTORY.rst

python-py 1.8.0 (1.7.0):
  http://py.readthedocs.io/en/latest/changelog.html#changelog

python-pyparsing 2.4.0 (2.3.1):
  https://github.com/pyparsing/pyparsing/blob/master/CHANGES

python-pyqt5 5.12.1 (5.12):
  https://www.riverbankcomputing.com/news/

python-pytest 4.4.0 (4.2.0):
  https://docs.pytest.org/en/latest/changelog.html

python-pytz 2019.1 (2018.9):
  https://git.launchpad.net/pytz/tree/tz/NEWS

python-pytzdata 2019.1 (2018.9):
  https://github.com/sdispater/pytzdata/commits/master

python-pyyaml 5.1 (3.13):
  https://github.com/yaml/pyyaml/blob/master/CHANGES

python-ruamel.yaml 0.15.91 (0.15.87):
  https://bitbucket.org/ruamel/yaml

python-setuptools 41.0.0 (40.8.0):
  https://setuptools.readthedocs.io/en/latest/history.html

python-sip 4.19.16 (4.19.14):
  https://www.riverbankcomputing.com/news/

python-soupsieve 1.9 (1.7.3):
  https://github.com/facelessuser/soupsieve/releases

python-sphinx 2.0.1 (1.8.4):
  http://www.sphinx-doc.org/en/stable/changes.html

python-stevedore 1.30.1 (1.30.0):
  https://docs.openstack.org/stevedore/latest/user/history.html

qt5 5.12.2 (5.12.1):
  https://wiki.qt.io/Qt_5.12.2_Change_Files

readline 8.0.0 (7.0.5):
  https://tiswww.case.edu/php/chet/readline/CHANGES

sqlite 3.27.2 (3.26.0):
  https://www.sqlite.org/changes.html


Downgraded packages
-------------------


Postponed updates
-----------------

mariadb-connector-c 3.0.8 (3.0.3):
  Upgrade when we switch to mariadb 10.3.  Check if auth_gssapi-Plugin still
  works!

python-conda-build 3.17.6 (3.13.0):
  Conda-build now uses *lief* for binary inspection, but it doesn’t use the
  DSO whitelist from our recipes any longer.  This leads to un-ignorable
  build errors.

  See:

  - https://github.com/conda/conda-build/issues/3360
  - https://github.com/conda/conda-build/issues/3370
