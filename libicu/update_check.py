checker = 'webbot'
url = 'http://site.icu-project.org/download/'

def find_version(soup):
    div = soup.find('div', id='sites-canvas-main-content')
    table = div.find('table').find('table')
    cell = table('tr')[1]('td')[1]
    version = cell.find('a').text
    return version
