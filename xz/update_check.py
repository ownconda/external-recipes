checker = 'webbot'
url = 'http://tukaani.org/xz/'

import re

def find_version(soup):
    # Find version from this paragraph:
    # <h3>Stable</h3>
    # <p>
    # 5.2.2 was released on 2015-09-29.
    # </p>
    h3 = soup.find('h3', string='Stable')
    for elem in h3.next_siblings:
        if elem.name == 'p':
           text = elem.text
           break

    version = re.search(r'(\d+\.\d+\.\d+)', text).group(1)
    return version
