cat > site.cfg <<EOF
[DEFAULT]
library_dirs = $PREFIX/lib
include_dirs = $PREFIX/include

[atlas]
atlas_libs = openblas
libraries = openblas

[openblas]
libraries = openblas
library_dirs = $PREFIX/lib
include_dirs = $PREFIX/include

EOF

CFLAGS="-fopenmp -m64 -mtune=native -O3 -Wl,--no-as-needed"
CXXFLAGS="-fopenmp -m64 -mtune=native -O3 -Wl,--no-as-needed"
LDFLAGS="-ldl -lm"
FFLAGS="-fopenmp -m64 -mtune=native -O3"
$PYTHON -m pip install --no-deps .
