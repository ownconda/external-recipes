import os
import sys
import timeit
import time

import numpy
from numpy.random import random

import warnings
warnings.filterwarnings("ignore")
# Hide: /localhome/henning/solarbox/lib/python2.7/site-packages/numpy/linalg/linalg.py:1729:
# RuntimeWarning: overflow encountered in det return _umath_linalg.det(a, signature=signature).astype(result_t)

numpy.show_config()


def test_eigenvalue():
    """
    Test eigen value computation of a matrix
    """
    i = 500
    data = random((i,i))
    result = numpy.linalg.eig(data)

def test_svd():
    """
    Test single value decomposition of a matrix
    """
    i = 1000
    data = random((i,i))
    result = numpy.linalg.svd(data)
    result = numpy.linalg.svd(data, full_matrices=False)

def test_inv():
    """
    Test matrix inversion
    """
    i = 1000
    data = random((i,i))
    result = numpy.linalg.inv(data)

def test_det():
    """
    Test the computation of the matrix determinant
    """
    i = 1000
    data = random((i,i))
    result = numpy.linalg.det(data)


def test_dot():
    """
    Test the dot product
    """
    i = 1000
    a = random((i, i))
    b = numpy.linalg.inv(a)
    result = numpy.dot(a, b) - numpy.eye(i)



tests = [test_eigenvalue,
         test_svd,
         test_inv,
         test_det,
         test_dot]


def start_benchmark():
    print("Starting timing with numpy %s\nVersion: %s" % (numpy.__version__, sys.version))
    print("%20s : %10s" % ("Function", "Timing [ms]"))
    start_time = time.time()
    for fun in tests:
        t = timeit.Timer(stmt="%s()" % fun.__name__, setup="from __main__ import %s" % fun.__name__)
        res = t.repeat(repeat=3, number=1)
        timing =  1000.0 * sum(res)/len(res)
        print("%20s : %7.1f ms" % (fun.__name__, timing))
    total_time = time.time() - start_time
    print("=> Total time: %s seconds" % total_time)


if __name__ == '__main__':
    start_benchmark()

