checker = 'webbot'
url = 'https://www.sqlite.org/'

import re

def find_version(soup):
    # Find version from this paragraph:
    # <h3>Latest Release</h3>
    # <a href="releaselog/3_14_2.html">Version 3.14.2</a> (2016-09-12).
    span = soup.find('h3', string=re.compile('Latest Release'))
    for elem in span.next_siblings:
        if elem.name == 'a':
            text = elem.text
            break

    version = re.search(r'(\d+\.\d+\.\d+)', text).group(1)
    return version
