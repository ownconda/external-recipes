checker = 'webbot'
# url = 'http://bzip.org/'  # This website is offline
url = 'https://web.archive.org/web/20180801004107/http://www.bzip.org/'

import re

def find_version(soup):
    # Find version from this paragraph:
    # <p>... current version ... <b>1.0.6</b> ...</p>
    p_text = soup.find(string=re.compile('current version'))
    paragraph = p_text.parent
    version = paragraph.b.text
    return version
