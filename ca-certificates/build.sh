# Create the directory to hold the certificates.
mkdir -p "${PREFIX}/ssl"

# Copy the certificates from certifi.
cat "$RECIPE_DIR"/*.pem certifi/cacert.pem > "${PREFIX}/ssl/cacert.pem"
ln -fs "${PREFIX}/ssl/cacert.pem" "${PREFIX}/ssl/cert.pem"
