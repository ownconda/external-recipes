./configure --prefix=$PREFIX --disable-all-programs --enable-libuuid

make
make check
make install

rm -rf $PREFIX/share
