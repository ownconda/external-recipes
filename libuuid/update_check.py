checker = 'webbot'
url = 'https://sourceforge.net/projects/libuuid/files/'

import re

def find_version(soup):
    link = soup.find('a', class_='download')
    text = link.find('span', class_='sub-label').text
    version = re.search(r'(\d+\.\d+\.\d+)', text).group(1)
    return version
