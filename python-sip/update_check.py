checker = 'webbot'
url = 'https://www.riverbankcomputing.com/software/sip/download'

import re

def find_version(soup):
    filename = soup.find('table', class_='download').find('a').text
    match = re.match(r'sip-(\d+\.\d+(\.\d+)?).(tar.gz|zip)', filename)
    return match.group(1)
