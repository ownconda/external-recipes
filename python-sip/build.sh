$PYTHON configure.py --sip-module PyQt5.sip
make
make install

# # Write metadata to make it detectable by pkg_resources
# DIST_INFO=$SP_DIR/$PKG_NAME-$PKG_VERSION.dist-info
# mkdir $DIST_INFO
# cat <<EOF > $DIST_INFO/METADATA
# Metadata-Version: 1.1
# Name: $PKG_NAME
# Version: $PKG_VERSION
# EOF
