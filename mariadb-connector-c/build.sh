mkdir build
cd build

cmake \
    -DCMAKE_BUILD_TYPE=Release \
    -DCMAKE_INSTALL_PREFIX=$PREFIX \
    -DINSTALL_LIBDIR=lib \
    -DINSTALL_PLUGINDIR=lib/plugin \
    -DMARIADB_UNIX_ADDR=/var/lib/mysql/mysql.sock \
    -DWITH_MYSQLCOMPAT=ON \
    -DAUTH_GSSAPI=STATIC \
    ..

# Use this for 3.0.4 and above:
    # -DAUTH_GSSAPI_PLUGIN_TYPE=STATIC \

make
make install

# WITH_MYSQLCOMPAT only creates links for the libs, not for the binary:
cd $PREFIX/bin
ln -s mariadb_config mysql_config
