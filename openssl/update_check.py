checker = 'webbot'
url = 'https://www.openssl.org/source/'
base_version = '1.1.1'

import re

def find_version(soup):
    dl_table = soup.find('div', id='content').find('table').text
    re_version = re.compile(r'({}\w*)\.tar\.gz'.format(re.escape(base_version)))
    return re_version.search(dl_table).group(1)
