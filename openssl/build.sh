if [[ "$(uname)" == Linux && -f /etc/centos-release ]]; then
    # The Perl version shipped with Centos7 is too old ...
    # yum install -y rh-perl526
    source /opt/rh/rh-perl526/enable
fi

if [[ "$(uname)" == Darwin ]]; then
    ./Configure darwin64-x86_64-cc shared --prefix=$PREFIX --openssldir=$PREFIX/ssl
else
    ./config shared --prefix=$PREFIX --openssldir=$PREFIX/ssl
fi

make --quiet

if [[ "$(whoami)" == root ]]; then
    # Some tests don't like to be run as "root".  Create a "test" user for them.
    USER='test'
    id -u $USER &>/dev/null || adduser $USER
    chown -R $USER .
    if [[ "$(uname)" == Linux && -f /etc/centos-release ]]; then
        # The Perl version shipped with Centos7 is too old ...
        /bin/sudo -u $USER bash -c "source /opt/rh/rh-perl526/enable && make test"  # Run tests
    else
        /bin/sudo -u $USER bash -c "make test"  # Run tests
    fi
    chown -R root .
else
    make test  # Run tests
fi

make install > /dev/null
