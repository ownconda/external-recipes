./configure --prefix=$PREFIX || { cat config.log; exit 1; }
make SHLIB_LIBS="$(pkg-config --libs ncursesw)"
make install

rm -rf $PREFIX/share/man
rm -rf $PREFIX/share/readline
