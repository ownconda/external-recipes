checker = 'custom'


# readline only has a file for the base release and for each patch only
# accompanying patch files:
#
#   readline-6.3.tar.gz
#   readline-6.3-patches/
#     readline63-001
#     readline63-001.sig
#     readline63-002
#     readline63-002.sig
#
# From this, we construct a version like "6.3.2".
import re

from pip._vendor.packaging.version import parse as parse_version
import ftplib
import urllib.parse


url = 'ftp://ftp.gnu.org/gnu/readline/'
regexp_release = r'readline-(\d+\.\d+).tar.gz'
regexp_patch = r'readline{}{}-(\d+)'
patch_dir = 'readline-{}-patches'


def get_latest_version():
    u = urllib.parse.urlparse(url)

    ftp = ftplib.FTP(u.netloc)
    ftp.login()
    ftp.cwd(u.path)

    # Get the version of the latest base release (e.g., "6.3")
    files = []
    ftp.retrlines('NLST', files.append)

    re_files = re.compile(regexp_release)
    version = parse_version('0')
    for f in files:
        match = re_files.match(f)
        if match is not None:
            version = parse_version(match.group(1))
            version = max(version, version)

    version = str(version)

    # Try to open the patch directory and find the highest patch number
    try:
        ftp.cwd(patch_dir.format(version))
    except ftplib.error_perm:
        patch_level = 0
    else:
        files = []
        ftp.retrlines('NLST', files.append)
        re_files = re.compile(regexp_patch.format(*version.split('.')))
        patch_level = 0
        for f in files:
            match = re_files.match(f)
            if match is not None:
                patch_level = max(int(match.group(1)), patch_level)

    return '{}.{}'.format(version, patch_level)
