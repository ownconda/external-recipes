checker = 'webbot'
url = 'http://www.libpng.org/pub/png/libpng.html'

import re

def find_version(soup):
    text = soup.find(string=re.compile(r'current public release')).next.text
    version = re.search(r'libpng (\d+\.\d+\.\d+)', text).group(1)
    return version
