checker = 'webbot'
url = 'http://zlib.net/'

import re

def find_version(soup):
    # Find version from this paragraph:
    # <CENTER>
    #     Current release:
    #
    #     <P>
    #     <FONT SIZE="+2"><B> zlib 1.2.8</B></FONT>
    #     <p>April 28, 2013
    # </CENTER>
    text = soup.find('center').find('p').find('font').find('b').text
    version = re.search(r'(\d+\.\d+\.\d+)', text).group(1)
    return version
