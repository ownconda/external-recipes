export LDFLAGS=-L${PREFIX}/lib
export CFLAGS=-I${PREFIX}/include

sed -i.orig s:'@PREFIX@':"${PREFIX}":g src/fccfg.c

bash ./configure \
    --prefix "${PREFIX}" \
    --enable-libxml2 \
    --enable-static \
    --disable-docs

make
make check
make install

# Remove computed cache with local fonts
rm -Rf "${PREFIX}/var/cache/fontconfig"

# Leave cache directory, in case it's needed
mkdir -p "${PREFIX}/var/cache/fontconfig"
touch "${PREFIX}/var/cache/fontconfig/.leave"
